﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Utils;

namespace PhonegapSpaDemo.api
{
    public class AddTestClaimsModule : IHttpModule
    {
        private string _claimTypesString;
        private const string _ClaimTypeSettingsKey = "testClaims:ClaimTypes";

        private string _claimValuesString;
        private const string _ClaimValueSettingsKey = "testClaims:ClaimValues";

        private ClaimsIdentity _testIdentity;

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            this._claimTypesString = ConfigurationManager.AppSettings.GetWithException(_ClaimTypeSettingsKey);

            this._claimValuesString = ConfigurationManager.AppSettings.GetWithException(_ClaimValueSettingsKey);

            List<Claim> testClaims =
                this._claimTypesString.Split(',')
                    .Zip(this._claimValuesString.Split(','), (type, value) => new Claim(type.Trim(), value.Trim()))
                    .ToList();

            this._testIdentity = new ClaimsIdentity(testClaims);

            context.AuthenticateRequest -= this.Context_AuthenticateRequest;
            context.AuthenticateRequest += this.Context_AuthenticateRequest;
        }

        private void Context_AuthenticateRequest(object sender, EventArgs args)
        {
            ClaimsPrincipal claimsPrincipal = ClaimsUtil.AddClaimsToThreadPrincipal(this._testIdentity);

            HttpContext.Current.User = claimsPrincipal;
        }
    }
}