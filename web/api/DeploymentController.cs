﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using PhonegapSpaDemo.config;

namespace PhonegapSpaDemo.api
{
	[RoutePrefix("api/deployment")]
	public class DeploymentController : ApiController
	{
		[Route("version")]
		public DeploymentInfo GetVersion()
		{
			//HACK
			HttpRequestBase request = new HttpRequestWrapper(HttpContext.Current.Request);

			return new DeploymentInfo
			{
				//TODO: get version from settings.js inside native install packages

				Version = ClientDeploymentConfig.CurrentClientVersion,
				PathToAndroid = ClientDeploymentConfig.GetUriForAndroidNativeClientInstaller(base.Request.RequestUri),
				PathToIos = ClientDeploymentConfig.GetUriForIosNativeClientInstaller(request)
			};
		}
		
		[Route("cordova")]
		public HttpResponseMessage GetCordova()
		{
			var result = new HttpResponseMessage(HttpStatusCode.OK);

			string downloadFilename = string.Format("cordova-{0}.zip", ClientDeploymentConfig.CurrentClientVersion);

			var stream = new FileStream(ClientDeploymentConfig.PathToCordovaZipFile, FileMode.Open);

			result.Content = new StreamContent(stream);

			result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");

			ContentDispositionHeaderValue contentDispositionHeaderValue = new ContentDispositionHeaderValue("attachment")
			{
				FileName = downloadFilename
			};

			result.Content.Headers.ContentDisposition = contentDispositionHeaderValue;

			return result;
		}
	}

	public class DeploymentInfo
	{
		public string Version { get; set; }
		public Uri PathToAndroid { get; set; }
		public Uri PathToIos { get; set; }
	}
}