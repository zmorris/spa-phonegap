﻿using System.Web.Http;
using Microsoft.Owin;
using Owin;
using PhonegapSpaDemo.config;

[assembly: OwinStartup(typeof(PhonegapSpaDemo.Startup))]

namespace PhonegapSpaDemo
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			WebApiConfig.Register(GlobalConfiguration.Configuration);
			
			
		}
	}
}