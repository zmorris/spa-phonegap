﻿using System;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Xml.Linq;
//[assembly: WebActivator.PreApplicationStartMethod(typeof (ClientDeploymentConfig), "Start")]
using Utils;

namespace PhonegapSpaDemo.config
{

	public class ClientDeploymentConfigModule : IHttpModule
	{
		private static bool _IsInitialized;

		public void Init(HttpApplication context)
		{
			context.BeginRequest -= ContextOnBeginRequest;
			context.BeginRequest += ContextOnBeginRequest;
		}

		private void ContextOnBeginRequest(object sender, EventArgs eventArgs)
		{
			if (_IsInitialized) return;

			_IsInitialized = true;

			HttpContext httpContext = ((HttpApplication)sender).Context;

			ClientDeploymentConfig.UpdateClientSettings(new HttpRequestWrapper(httpContext.Request));
		}

		public void Dispose()
		{
		}
	}

    /// <summary>
    ///     Update version and host URL settings for SPA client(s) based on current web.config and application host.
    /// </summary>
    public static class ClientDeploymentConfig
    {
        private static string _ClientVersion;
        private const string _ClientVersionSettingsKey = "clientDeployment:ClientVersion";

        //TODO: move to web.config
        private const string _ClientSettingsFileName = "deploymentSettings.js";
        private const string _RelativePathToClientSettingsFile = "client/app/" + _ClientSettingsFileName;

        private static Uri _UriToAndroidNativeClientInstaller;
        private const string _RelativePathToAndroidApk = "client-native/deploy/android/spademo.apk";

        private static Uri _UriToIosNativeClientInstaller;
        private const string _RelativePathToIosDeployAssets = "client-native/deploy/ios/";
		private const string _RelativePathToiOSipaFile = _RelativePathToIosDeployAssets + "spademo.ipa";
        private const string _RelativePathToiOSsmallImageFile = _RelativePathToIosDeployAssets + "icon.png";
        private const string _RelativePathToiOSlargeImageFile = _RelativePathToIosDeployAssets + "iTunesArtwork.png";
        private static bool _IsIosPlistUpdated;

        private const string _RelativePathToCordovaClientFiles = "client/";
        private const string _RelativePathToCordovaConfig = _RelativePathToCordovaClientFiles + "config.xml";
        private const string _RelativePathToCordovaBuildZip = "client-native/build/cordova.zip";

//        public static void Start()
//        {
//            HttpApplication.RegisterModule(typeof (ClientDeploymentConfigModule));
//        }

        public static void UpdateClientSettings(HttpRequestBase request)
        {
            UpdateClientSettingsForWebClient(request);

            AssembleCordovaBuildPackage(request);
        }

        private static void AssembleCordovaBuildPackage(HttpRequestBase request)
        {
            UpdateCordovaConfigFile(request, _RelativePathToCordovaConfig, CurrentClientVersion);

            BuildCordovaZipFile(_RelativePathToCordovaClientFiles,
                                request.PhysicalApplicationPath,
                                _RelativePathToCordovaBuildZip);
        }

        private static void BuildCordovaZipFile(string relativePathToCordovaClientFiles,
                                                string physicalApplicationPath,
                                                string relativePathToCordovaBuildZip)
        {
            string pathToClientFiles = Path.Combine(physicalApplicationPath, relativePathToCordovaClientFiles);

            PathToCordovaZipFile = Path.Combine(physicalApplicationPath, relativePathToCordovaBuildZip);

            if (File.Exists(PathToCordovaZipFile))
            {
                File.Delete(PathToCordovaZipFile);
            }

            ZipFile.CreateFromDirectory(pathToClientFiles, PathToCordovaZipFile);

            using (ZipArchive archive = ZipFile.Open(PathToCordovaZipFile, ZipArchiveMode.Update))
            {
                ZipArchiveEntry entry = archive.Entries.FirstOrDefault(e => e.Name == "cordova.js");

                if (entry != null)
                {
                    entry.Delete();
                }
            }
        }

        public static string PathToCordovaZipFile { get; private set; }

        private static void UpdateCordovaConfigFile(HttpRequestBase request,
                                                    string relativePathToCordovaConfig,
                                                    string currentClientVersion)
        {
            string physicalApplicationPath = request.PhysicalApplicationPath;

            string pathToCordovaConfigFile = Path.Combine(physicalApplicationPath, relativePathToCordovaConfig);

            XElement configXml = XElement.Load(pathToCordovaConfigFile);

            configXml.Attribute("version").Value = currentClientVersion;

            configXml.Save(pathToCordovaConfigFile);
        }

        private static void UpdateClientSettingsForWebClient(HttpRequestBase request)
        {
            string clientSettingsFileContents = BuildClientSettingsFileContents(request, CurrentClientVersion);

            string relativePathToClientSettingsFile = _RelativePathToClientSettingsFile;

            string pathToClientSettingsFile = Path.Combine(request.PhysicalApplicationPath,
                                                           relativePathToClientSettingsFile);

            using (StreamWriter writer = new StreamWriter(pathToClientSettingsFile, false))
            {
                writer.Write(clientSettingsFileContents);

                writer.Flush();
            }
        }

        private static string BuildClientSettingsFileContents(HttpRequestBase request, string currentClientVersion)
        {
            string appRoot = GetAppRoot(request).ToString();

            appRoot = appRoot.TrimEnd('/');

            string clientVersion = currentClientVersion;

            string clientSettingsFileContents =
                string.Format("define(function() {{ return {{hostUrl: '{0}' ,version: '{1}'}};}});",
                              appRoot,
                              clientVersion);

            return clientSettingsFileContents;
        }

        internal static string CurrentClientVersion
        {
            get
            {
                if (_ClientVersion == null)
                {
                    _ClientVersion = ConfigurationManager.AppSettings.GetWithException(_ClientVersionSettingsKey);
                }

                return _ClientVersion;
            }
        }

        internal static Uri GetUriForIosNativeClientInstaller(HttpRequestBase request)
        {
            if (_UriToIosNativeClientInstaller == null)
            {
                string relativePathToPlistFile = _RelativePathToIosDeployAssets + "app.plist";

                Uri requestUrl = request.Url;

                _UriToIosNativeClientInstaller = new Uri(
                    string.Format("{3}{0}://{1}/{2}",
                                  requestUrl.Scheme,
                                  requestUrl.Authority,
                                  relativePathToPlistFile,
                                  "itms-services://?action=download-manifest&url="));

                if (!_IsIosPlistUpdated)
                {
                    UpdatePlistFile(request, relativePathToPlistFile, CurrentClientVersion);

                    _IsIosPlistUpdated = true;
                }
            }

            return _UriToIosNativeClientInstaller;
        }

        private static void UpdatePlistFile(HttpRequestBase request,
                                            string relativePathToPlistFile,
                                            string clientVersion)
        {
            string currentClientVersion = clientVersion;

            string physicalApplicationPath = request.PhysicalApplicationPath;

            string pathToPlistFile = Path.Combine(physicalApplicationPath, relativePathToPlistFile);

            XElement plistXml = XElement.Load(pathToPlistFile);

            UpdateIosPlistVersion(currentClientVersion, plistXml);

            UpdateIosPlistDeploymentPaths(plistXml, request);

            plistXml.Save(pathToPlistFile);
        }

        private static void UpdateIosPlistVersion(string version, XElement plistXml)
        {
            XElement keyElement = GetDescendentElement(plistXml, "bundle-version");

            XElement versionElement = keyElement.ElementsAfterSelf().First();

            versionElement.SetValue(version);
        }

        private static void UpdateIosPlistDeploymentPaths(XElement plistXml, HttpRequestBase request)
        {
            Uri hostUri = GetAppRoot(request);

            XElement keyElement = GetDescendentElement(plistXml, "software-package");

            XElement urlElement = GetNextElement(keyElement, "url").ElementsAfterSelf().First();

            Uri pathToIpaFile = new Uri(hostUri, _RelativePathToiOSipaFile);

            urlElement.SetValue(pathToIpaFile.ToString());


            keyElement = GetDescendentElement(plistXml, "display-image");

            urlElement = GetNextElement(keyElement, "url").ElementsAfterSelf().First();

            Uri pathToSmallIconFile = new Uri(hostUri, _RelativePathToiOSsmallImageFile);

            urlElement.SetValue(pathToSmallIconFile.ToString());


            keyElement = GetDescendentElement(plistXml, "full-size-image");

            urlElement = GetNextElement(keyElement, "url").ElementsAfterSelf().First();

            Uri pathToLargeIconFile = new Uri(hostUri, _RelativePathToiOSlargeImageFile);

            urlElement.SetValue(pathToLargeIconFile.ToString());
        }

        private static Uri GetAppRoot(HttpRequestBase request)
        {
            Uri hostUri = new Uri(string.Format("{0}://{1}",
                                                request.Url.Scheme,
                                                request.Url.Authority));

            hostUri = new Uri(hostUri, request.ApplicationPath);

            return hostUri;
        }

        private static XElement GetDescendentElement(XElement startElement, string elementValue)
        {
            XElement keyElement = startElement.Descendants().FirstOrDefault(e => e.Value == elementValue);

            if (keyElement == null)
            {
                throw new ApplicationException(string.Format("'{0}' element not found.", elementValue));
            }

            return keyElement;
        }

        private static XElement GetNextElement(XElement startElement, string elementValue)
        {
            XElement element = startElement.ElementsAfterSelf().FirstOrDefault(e => e.Value == elementValue);

            if (element == null)
            {
                throw new ApplicationException(string.Format("'{0}' element not found.", elementValue));
            }

            return element;
        }

        internal static Uri GetUriForAndroidNativeClientInstaller(Uri requestUri)
        {
//            Uri requestUrl = request.Url;

            if (_UriToAndroidNativeClientInstaller == null)
            {
                _UriToAndroidNativeClientInstaller = new Uri(string.Format("{0}://{1}/{2}",
                                                                           requestUri.Scheme,
                                                                           requestUri.Authority,
                                                                           _RelativePathToAndroidApk));
            }

            return _UriToAndroidNativeClientInstaller;
        }
    }
}