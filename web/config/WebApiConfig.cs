﻿using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace PhonegapSpaDemo.config
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
//            config.SuppressDefaultHostAuthentication();
//            config.Filters.Add(new HostAuthenticationFilter(Startup.OAuthOptions.AuthenticationType));

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());

            // Use camel case for JSON data.
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Uncomment the following line of code to enable a default non-attribute route.
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //TODO: enable compression for application/json globally in IIS: http://stackoverflow.com/questions/2775261/how-to-enable-gzip-http-compression-on-windows-azure-dynamic-content/7375645#7375645
            GlobalConfiguration.Configuration.MessageHandlers.Add(new CompressionDelegateHandler());

			config.EnsureInitialized();
        }
    }
}
