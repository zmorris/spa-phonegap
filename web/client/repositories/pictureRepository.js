﻿define(['moment', 'knockout', 'Q', 'jquery', 'linq', 'app/services'],
	function(moment, ko, Q, jquery, linq, _appServices) {

		var _getTankHistoryForDay = function(localDate, tankId) {

			var dateUtc = moment.utc(localDate);

			var url = _appServices.config.hostUrl + 'api/tanks/' + tankId + '/history/' + dateUtc.format("YYYY/MM/DD");

			return Q.when(jquery.ajax(url))
				.catch(function(jqXHR, textStatus, errorThrown) {

					throw _appServices.errorHandler.buildError(jqXHR, textStatus, errorThrown);
				})
				.then(function(tankHistory) {

					//converter JSON "NaN" to literal NaN
					tankHistory.forEach(function (reading) {
						
						for (var index = 0; index < reading.v.length; index++) {

							var value = reading.v[index];

							reading.v[index] = value == "NaN" ? NaN : value;
						}
					});

					return tankHistory;
				});
		};

		var _getNDaysHistory = function (tankId, days, endTime) {

			var promises = [];

			endTime = endTime == undefined ? moment().utc() : endTime;
			
			for (var daysAgo = days; daysAgo >= 0; daysAgo--) {

				var daysAgoUtc = endTime.clone().subtract('days', daysAgo).startOf('day');

				promises.push(_getTankHistoryForDay(daysAgoUtc, tankId));
			}

			return Q.all(promises)
				.then(function(histories) {

					var oldestHistory = histories[histories.length - 1];

					var maxReadingAge = endTime.clone().subtract('days', days);

					histories[histories.length - 1] = linq.From(oldestHistory)
						.Where(function(reading) {

							var readingTime = moment.utc(reading.t);
							
							return readingTime >= maxReadingAge  && readingTime <= endTime;
						})
						.ToArray();

					var completeHistory = [];

					histories.forEach(function(history) {

						completeHistory = completeHistory.concat(history);
					});

					return completeHistory;
				});
		};

		var _getChannelDescriptions = function(tankId) {

			var url = _appServices.config.hostUrl + 'api/tanks/' + tankId + '/history/description';

			return Q.when(jquery.ajax(url))
				.catch(function(jqXHR, textStatus, errorThrown) {

					throw _appServices.errorHandler.buildError(jqXHR, textStatus, errorThrown);
				})
				.then(function(channelDescriptions) {

					return channelDescriptions.sort(function(a, b) {
						return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
					});
				});
		};

		var _setChannelHistories = function(tankHistory, channelDescriptions) {
			
			for (var index = 0; index < channelDescriptions.length; index++) {

				var channelDescription = channelDescriptions[index];
				
				var channelHistory = [];

				var hasValues = false;
				
				var getChannelValue = function (reading) {
					
					var value = parseFloat(reading.v[channelDescription.index]);
					
					if (!isNaN(value)) {
						hasValues = true;
					}
					
					return value;
				};
								
				tankHistory.forEach(function (reading) {
					
					channelHistory.push([reading.t, getChannelValue(reading)]);
				});

//				_addNaNForMissingValuesInChannelHistory(channelHistory);
				
				channelDescription.history = hasValues ? channelHistory : [];
			}
		};
		
			
		var _addNaNForMissingValuesInChannelHistory = function (channelHistory) {

			var readingPeriods = _getPeriodsBetweenReadings(channelHistory);

			var nominalReadingPeriod = _appServices.math.mode(readingPeriods);

			for (var index = 0; index < channelHistory.length; index++) {

				var reading = channelHistory[index];

				var nextPeriod = readingPeriods[index];
				
				if (nextPeriod && nextPeriod > nominalReadingPeriod) {

					channelHistory.splice(index + 1, 0, [reading[0] + 1, null]);
//					channelHistory.splice(index, 0, [reading[0] - 2, null]);
				};
			}
		};

		var _getPeriodsBetweenReadings = function (channelHistory) {

			var periods = [];
			
//			var maxIndex = Math.min(channelHistory.length - 1, 1000);
			var maxIndex = channelHistory.length - 1;

			for (var index = 0; index < maxIndex; index++) {

				var periodMinutes = Math.floor(((channelHistory[index + 1][0] - channelHistory[index][0]) / 60000));

				periods.push(periodMinutes);
			}

			return periods;
		};

		var _getHistoryAsCsv = function(tankHistory, channelDescriptions) {

			var historyCsv = '';
			
			var headerRow = 'time,';

			channelDescriptions.forEach(function(channel) {
				headerRow += channel.name + ' ' + channel.units +',';
			});

			historyCsv += headerRow + '\n';

			tankHistory.forEach(function(reading) {

				var row = '';

				row += moment(reading.t).format("YYYY-MM-DD hh:mm:ss Z") + ',';

				reading.v.forEach(function(value) {

					row += value + ',';
				});

				historyCsv += row + '\n';
			});
			
			return historyCsv;
		};


		return {
			getTankHistoryForDay: _getTankHistoryForDay,
			getNDaysHistory: _getNDaysHistory,
			getChannelDescriptions: _getChannelDescriptions,
			setChannelHistories: _setChannelHistories,
			getHistoryAsCsv: _getHistoryAsCsv,
		};
	});