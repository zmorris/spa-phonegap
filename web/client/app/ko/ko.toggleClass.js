﻿
define(['knockout', 'jquery'],
	function (ko, $) {

		ko.bindingHandlers.toggleClass = {
			init: function (element, valueAccessor, allBindingsAccessor, context) {

				var options = valueAccessor();

				var parentSelector = options.parentSelector;
				var classToToggle = options.classToToggle;
				var delayMs = (options.delayS || 0) * 1000;
				var removeOnLeave = options.removeOnLeave || false;
				
				var newValueAccessor = function () {
					
					return function() {

						window.setTimeout(function() {

							if (parentSelector) {
								$(element).parents(parentSelector).toggleClass(classToToggle);
							} else {
								$(element).toggleClass(classToToggle);
							}

						}, delayMs);
					};
				};

				if (removeOnLeave) {

					$(element).mouseleave(function () {
						$(element).removeClass(classToToggle);
					});

					//TODO: this doesn't work?
					$('.tile').on('touchstart'), function () {
						$(element).removeClass(classToToggle);
					};
				}

				ko.bindingHandlers.click.init(element, newValueAccessor, allBindingsAccessor, context);
			}
		};
	});