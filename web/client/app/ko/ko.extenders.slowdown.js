﻿
define(['knockout', 'Q'],
	function (ko, Q) {

		ko.extenders.slowdown = function (source, options) {
			
			source.previousUpdate = Q.delay(1);

			source.subscribe(function (newValue) {

				var updateProperty = function () {
					options.target(newValue);
				};

				source.previousUpdate
					.then(function () {
						source.previousUpdate = Q.delay(options.minChangeRateMilliseconds).then(updateProperty);
					});
			});
		};
	});