﻿
define(['knockout', 'moment'],
	function (ko, moment) {

	ko.bindingHandlers.dateRange = {
		update: function(element, valueAccessor) {
			
			var range = valueAccessor();

			if (range) {

				var dateFormat = 'MMM D hh:mm a';

				element.textContent = moment(range[0]).local().format(dateFormat)
					+ ' - ' + moment(range[1]).local().format(dateFormat);
			} else {
				element.textContent = '';
			}
		},
	};
});