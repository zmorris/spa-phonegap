﻿
define(['knockout', 'jquery'],
	function(ko, $) {

		ko.bindingHandlers.addClassAfterDelay = {
			init: function(element, valueAccessor, allBindingsAccessor, context) {

				var options = valueAccessor();

				var parentSelector = options.parentSelector;
				var classToToggle = options.classToAdd;
				var delayMs = options.delayS * 1000;

				window.setTimeout(function() {

					if (parentSelector) {
						$(element).parents(parentSelector).toggleClass(classToToggle);
					} else {
						$(element).toggleClass(classToToggle);
					}

				}, delayMs);

			}
		};
	});