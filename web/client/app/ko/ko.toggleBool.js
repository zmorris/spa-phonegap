﻿
define(['knockout'],
	function(ko) {

		ko.bindingHandlers.toggleBool = {
			init: function(element, valueAccessor, allBindingsAccessor, context) {

				var booleanObservable = valueAccessor();

				var newValueAccessor = function() {

					return function() {

						if (booleanObservable()) {
							booleanObservable(false);
						} else {
							booleanObservable(true);
						}
					};
				};

				ko.bindingHandlers.click.init(element, newValueAccessor, allBindingsAccessor, context);
			}
		};
	});