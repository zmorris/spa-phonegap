﻿define(['app/errorHandler'],
	function(_errorHandler) {

		var _successCallback;

		var _onPhotoUriSuccess = function(localFileUri) {

			if (_successCallback) {
				_successCallback(localFileUri);
			}
		};

//        var _onPhotoDataSuccess = function(imageData) {
//
//            _exceptionHandler.handleErr("camera DATA succeeded");
//            
//            var newPhoto = _photoRepository.createNewPhoto(_contact().id(), new Date());
//
//            newPhoto.imageData(imageData);
//
//            _photos.push(newPhoto);
//        };

		var _onPhotoFail = function(message) {

			_errorHandler.handleErr("camera failed: " + message);
		};

		var _takePicture = function(successCallback) {

			_successCallback = successCallback;
			
//            navigator.camera.getPicture(_onPhotoDataSuccess, _onPhotoFail, {
//                quality: 50,
//                destinationType: navigator.camera.DestinationType.DATA_URL
//            });
			navigator.camera.getPicture(_onPhotoUriSuccess, _onPhotoFail, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI
			});
		};
		
		var _isCameraAvailable = function() {
			return navigator.camera;
		};
		
		return {
			isCameraAvailable: _isCameraAvailable,
			takePicture: _takePicture
		};
	});