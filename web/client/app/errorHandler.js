﻿define(['durandal/events', 'durandal/app', 'durandal/system', 'knockout'],
	function(_durandalEvents, _durandalApp, _durandalSystem, ko) {

		var _showStack = false;

		var _onError = function(error, message, isWarning) {

			if (typeof error == 'string') {

				error = { message: error };
			}
			
			if (message == undefined) {

				message = error.message;
			}

			if (_isNetworkFailureError(error)) {

				message = message + ': ' + 'Server unavailable.';
			}

			if (_isUnauthorizedUrlError(error)) {

				_onUnauthorizedUrlError();

				return;
			}

			error.status = error.status == undefined ? '' : error.status;

			error.statusText = error.statusText == undefined ? '' : error.statusText;

			if (error.statusText && error.status) {

				message = message + ' (' + error.statusText + ' : ' + error.status + ')';
			}

			//            if (error.breezeValidationErrors != undefined) {
			//
			//                if (error.breezeValidationErrors.length > 0) {
			//
			//                    error.breezeValidationErrors.forEach(function(validationError) {
			//
			//                        message += ' ' + validationError.errorMessage;
			//                    });
			//                }
			//            }

			if (_showStack && error.stack) {

				message = message + '(stack: ' + error.stack + ')';
			}

			if (error.status == 500) {

				message = "A problem occured while retieving your data. Please try again later.";
			}

			_processMessage(message, isWarning);
		};

		//        var _getBreezeValidationErrors = function(breezeError) {
		//
		//            var validationErrors = [];
		//
		//            try {
		//
		//                if (breezeError.entitiesWithErrors != undefined) {
		//
		//                    breezeError.entitiesWithErrors.forEach(function(entity) {
		//
		//                        var errors = entity.entityAspect.getValidationErrors();
		//
		//                        errors.forEach(function(validationError) {
		//
		//                            validationErrors.push(validationError);
		//                        });
		//                    });
		//                }
		//
		//            } catch(err) {
		//
		//                _logger.logError(err.message);
		//            }
		//
		//            breezeError.breezeValidationErrors = validationErrors;
		//
		//            return validationErrors;
		//        };

		var _lastErrorMessage = ko.observable('');
		
		var _filterErrorsToDisplay = function (errorMessage) {

			//HACK: implement pluggable error policies
			if (errorMessage.indexOf('Highcharts error #19') >= 0) {
				return false;
			}

			return true;
		};
		
		var _processMessage = function(message, isWarning) {
			if (!isWarning) {

				if (_filterErrorsToDisplay(message)) {
					_lastErrorMessage(message);
				}
				
			}
			
			_durandalSystem.log('Error: ' + message);

//			_durandalApp.showMessage(message, "An Error Occurred");
		};

		var _isNetworkFailureError = function(error) {

			return error.status == 0;
		};

		var _isUnauthorizedUrlError = function(error) {

			return error.status == 401;
		};

		var _buildError = function(XHR, textStatus, errorThrown) {

			var err = new Error();

			err.XHR = XHR;
			err.responseText = XHR.responseText;
			err.status = XHR.status;
			err.statusText = XHR.statusText;
			err.message = XHR.statusText;

			return err;
		};

		var _unauthorizedEvent = 'unauthorized';

		var _errorHandler = {
			onError: _onError,
			//            showWarning: _showWarning,
			//            preserveBreezeValidationErrors: _getBreezeValidationErrors,
			isNetworkFailureError: _isNetworkFailureError,
			buildError: _buildError,
			unauthorizedEvent: _unauthorizedEvent,
			lastErrorMessage: _lastErrorMessage
		};

		_durandalEvents.includeIn(_errorHandler);

		var _onUnauthorizedUrlError = function() {

			_errorHandler.trigger(_unauthorizedEvent);
		};

		return _errorHandler;
	});