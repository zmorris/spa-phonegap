﻿define(['jquery',
		'app/ko/ko.addClassAfterDelay',
		'app/ko/ko.toggleClass',
		'app/ko/ko.dateRange',
		'app/ko/ko.extenders.slowdown',
		'app/ko/ko.toggleBool',
		
	],
	function ($) {

			var _afterRenderApplyClass = function(arg1, arg2) {

				var index = this.foreach.indexOf(arg2);

				var delay = index * this.delay;

				var classToAdd = this.classToAdd;

				var container = arg1[1];

				//css transitions don't happen if class is set directly in this callback, use timeout to let knockout finish rendering
				window.setTimeout(function() {

					$(container).addClass(classToAdd);

				}, delay);
			};

			return {
				koAfterRender: {
					applyClass: _afterRenderApplyClass
				}
			};
		})