﻿define(['app/deploymentSettings', 'app/deviceListener'],
    function ( _deploymentSettings, _deviceListener) {

        var _signinUrl = _deploymentSettings.hostUrl + '/api/account/signinstatus';

        //TODO: get cordova  via requirejs
        var _isCordova = navigator.app ? true : false || document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

        var _getTimestamp = function () {

            return new Date().getTime() / 1000 - new Date("2013-01-01").getTime() / 1000;
        };
		
        var _init = function (doc) {

            _deviceListener.init(doc);
        };
	    
        if (!window.location.origin) {
        	window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }

        _deploymentSettings.hostUrl = window.location.origin + "/";
	    
	    return {
		    init: _init,
		    version: _deploymentSettings.version,
		    hostUrl: _deploymentSettings.hostUrl,
            updateCheckIntervalMinutes: 0.1,// 5.05,
            maxWaitIntervalForAllTasksFinished: 2000,
            defaultOperationTimeoutMilliseconds: 10000,
            isCordovaDeployment: _isCordova,
            getTimestamp: _getTimestamp,
            signinUrl: _signinUrl
        };
    });