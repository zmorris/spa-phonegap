﻿define(['app/services', 'plugins/router'],
	function (_appServices, _rootRouter) {

    var _replaceStringInRoutes = function (router, newValue, oldValue) {

        var currentModel = router.navigationModel();

        var newModel = [];

        var tankUrlEncodedName = encodeURIComponent(newValue);

        currentModel.forEach(function (routeConfig) {

            routeConfig.hash = routeConfig.hash.replace(encodeURIComponent(oldValue), tankUrlEncodedName);

            newModel.push(routeConfig);
        });

        router.navigationModel([]);
        router.navigationModel(newModel);
    };

	var _navigateToAppRoot = function() {
		try {

			_rootRouter.navigate('/');
		} catch(err) {
			_appServices.errorHandler.onError(err);
		}
	};
	
    return {
    	replaceStringInRoutes: _replaceStringInRoutes,
    	navigateToAppRoot: _navigateToAppRoot
    };
})