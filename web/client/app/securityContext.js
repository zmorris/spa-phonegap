﻿define(['knockout',
        'jquery',
        'Q',
        'app/appTasks',
        'app/errorHandler',
        'app/config'],
    function (ko, jquery, Q, _appTasks, _errorHandler, _appConfig) {

        //TODO: get reference via require
        var _localStorage = window.localStorage;
        var _userInfoStorageKey = 'securityContext:userInfo';

        var _userInfo = ko.observable({});

        var _isInitialized = false;

        var _initializeContext = function() {

            var url = _appConfig.hostUrl + 'api/account/user';

            return Q.when(jquery.ajax(url))
                .fail(function(jqXHR, textStatus, errorThrown) {

                    var err = _errorHandler.buildError(jqXHR, textStatus, errorThrown);

                    if (_errorHandler.isNetworkFailureError(err)) {

                        var serializedUserInfo = _localStorage.getItem(_userInfoStorageKey);

                        var userInfo = JSON.parse(serializedUserInfo);

                        return userInfo;

                    } else {

                        throw err;
                    }
                })
                .then(function(data) {

                    _setCurrentUserInfo(data);
                });
        };

        var _setCurrentUserInfo = function(data) {

            _userInfo(data);

            //cache context for offline mode
            var serializedUserInfo = JSON.stringify(data);

            _localStorage.setItem(_userInfoStorageKey, serializedUserInfo);

            _isInitialized = true;

        };

        return {
            initContext: _initializeContext,
            userInfo: _userInfo,
        };
    });