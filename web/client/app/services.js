﻿//TODO: composite module as single access point to app services: config, deployment, errHandler, tasks

define([
		'app/bindings',
		'app/config',
        'app/errorHandler',
        'app/appTasks',
        'app/securityContext',
        'app/deviceListener',
		'app/navigation'
    ],
    function (
	    _koBindings,
        _config,
        _errorHandler,
        _appTasks,
        _securityContext,
        _deviceListener,
	    _navigationHelper
    ) {

        return {
            config: _config,
            errorHandler: _errorHandler,
            appTasks: _appTasks,
            securityContext: _securityContext,
            deviceListener: _deviceListener,
            navigation: _navigationHelper,
        };
    });