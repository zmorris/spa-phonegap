﻿define(['knockout', 'Q', 'app/config'],
    function (ko, Q, _config) {

        var _tasks = ko.observableArray([]);

        var _currentTask = ko.computed(function () {

            var length = _tasks().length;

            return length < 1 ? undefined : _tasks()[length - 1];

        });

        var hasUiBlockingTasks = ko.computed(function () {

            var taskCount = _tasks().length;

            if (taskCount == 0) {

                return false;
            }

            return _tasks().filter(function (task) {
                return task.isBlocking;
            }).length > 0;

        });

        var _pushTask = function (message, isUiBlockingTask) {

            if (isUiBlockingTask == undefined) {

                isUiBlockingTask = true;
            }

            var task = {
                message: message,
                isBlocking: isUiBlockingTask
            };

            _tasks.push(task);

            return task;
        };

        var _popTask = function (task) {

            _tasks.remove(task);
        };

        var _waitForAllTasksToFinish = function (waitTimeout) {

            waitTimeout = waitTimeout ? waitTimeout : _config.maxWaitIntervalForAllTasksFinished;

            var deferred = Q.defer();

            deferred.timeoutToken = setTimeout(function () {

                try {

                    if (_tasks().length == 0) {

                        deferred.resolve(true);

                        clearTimeout(deferred.timeoutToken);
                    }

                } catch (err) {

                    clearTimeout(deferred.timeoutToken);

                    deferred.reject(err);
                }

            }, 100);

            return deferred.promise.timeout(waitTimeout);
        };

        return {
            pushTask: _pushTask,
            popTask: _popTask,
            currentTask: _currentTask,
            hasUiBlockingTasks: hasUiBlockingTasks,
            allTasks: _tasks, //TODO: make this read-only?
            waitForAllTasksToFinish: _waitForAllTasksToFinish
        };
    });