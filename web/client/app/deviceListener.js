﻿define(['knockout', 'app/errorHandler'],
    function (_ko, _errorHandler) {

        var _onAppPaused = function () {
            
//            _logger.log('app paused: ' + moment().format('h:mm:ss'), null, null, true);
//            console.log(_cordova.connection.type);
        };
        var _onAppActive = function () {
            
//            _logger.log('app active: ' + moment().format('h:mm:ss'), null, null, true);
//            console.log('app active');
//            console.log(_cordova.connection.type);

        };
        
        var _onAppResign = function () {

//            _logger.log('app resign: ' + moment().format('h:mm:ss'), null, null, true);
//            console.log('app resign');
//            console.log(_cordova.connection.type);

        };
       
        var _onAppResume = function () {
            
//            _logger.log('app resume: ' + moment().format('h:mm:ss'), null, null, true);
//            console.log('app resume');
//            console.log(_cordova.connection.type);

        };

        var _deviceOfflineCallbacks = [];
        var _onDeviceOfflineCallbacks = function(callback) {

            _deviceOfflineCallbacks.push(callback);
        };
        var _isOffline = _ko.observable(false);
        var _onAppOffline = function () {
            
//            _logger.log('app offline: ' + moment().format('h:mm:ss'), null, null, true);

            _isOffline(true);
            
            _deviceOfflineCallbacks.forEach(function(callback) {

                try {
                    callback();
                } catch(err) {
                    _errorHandler.onError(err);
                }
            });
        };

        var _deviceOnlineCallbacks = [];
        var _onDeviceOnlineCallbacks = function(callback) {
            
            _deviceOnlineCallbacks.push(callback);
        };
        var _onAppOnline = function () {

//            _logger.log('app online: ' + moment().format('h:mm:ss'), null, null, true);

            _isOffline(false);

            _deviceOnlineCallbacks.forEach(function(callback) {

                try {
                    callback();
                } catch(err) {
                    _errorHandler.onError(err);
                }
            });
        };

        var _onDeviceReady = function () {
            
//            _logger.log('_onDeviceReady: ' + moment().format('h:mm:ss'), null, null, true);
            
            document.addEventListener("pause", _onAppPaused, false);
            document.addEventListener("resign", _onAppResign, false);//iOS only

            document.addEventListener("resume", _onAppResume, false);
            document.addEventListener("active", _onAppActive, false); //iOS only

            document.addEventListener("offline", _onAppOffline, false);

            document.addEventListener("online", _onAppOnline, false);
        };

        //TODO: get document via requirejs
        var _init = function (document) {
            
            try {
                
                document.addEventListener("deviceready", _onDeviceReady, false);
                
            } catch(err) {
                _errorHandler.onError(err.message);
            }
        };
    
        return {
            init: _init,
            onDeviceOffline: _onDeviceOfflineCallbacks,
            onDeviceOnline: _onDeviceOnlineCallbacks,
            isOffline: _isOffline
        };
    });