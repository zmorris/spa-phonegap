﻿define(['jquery', 'knockout', 'Q', 'app/config', 'app/errorHandler'],
	function($, ko, Q, _config, _errorHandler) {

		var _isUpdateAvailable = ko.observable(false);
		var _deploymentInfo = ko.observable({});

		var _checkForUpdates = function() {

			try {

				Q.when($.ajax(_config.hostUrl + 'api/deployment/version'))
					.catch(function(XHR, textStatus, errorThrown) {

						var err = _errorHandler.buildError(XHR, textStatus, errorThrown);

						err.message = 'Check for updates failed.';

						if (_errorHandler.isNetworkFailureError(err)) {

							//ignore failure

						} else {

							_errorHandler.onError(err);
						}
					})
					.then(function (deploymentInfo) {

						try {

							_isUpdateAvailable(false); //force binding refresh

							_isUpdateAvailable(deploymentInfo.version != _config.version);

							_deploymentInfo(deploymentInfo);

						} catch(err) {
							_errorHandler.onError(err);
						}
					});
				
			} catch(error) {
				_errorHandler.onError(error);
			}
		};

		if (_config.isCordovaDeployment) {

			window.setTimeout(_checkForUpdates, 500);

			document.addEventListener("resume", _checkForUpdates, false);
			document.addEventListener("online", _checkForUpdates, false);

			window.setInterval(_checkForUpdates, _config.updateCheckIntervalMinutes * 60000);
		}

		return {
			isUpdateAvailable: _isUpdateAvailable,
			updateUrl: _config.updateUrl,
			version: _config.version,
			deploymentInfo: _deploymentInfo,
			checkForUpdates: _checkForUpdates,
		};

	});