﻿define(['knockout', 'app/services', 'app/deployment'],
	function (ko, _appServices, _deployment) {



		return {
			activate: function() {
				try {

					return _deployment.checkForUpdates();
					
				} catch(err) {
					_appServices.errorHandler.onError(err);
				}
			},
			deploymentInfo: _deployment.deploymentInfo,
			isUpdateAvailable: _deployment.isUpdateAvailable,
			currentVersion: _appServices.config.version
		};
	});