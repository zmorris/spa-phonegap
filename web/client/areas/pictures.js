﻿define(['knockout','app/services', 'app/takePicture'],
	function(ko, _appServices, _pictureTaker) {

		var _localPictures = ko.observableArray([]);

		var _takePicture = function() {

			try {

				_pictureTaker.takePicture(function(pictureUri) {
					_localPictures.push(pictureUri);
				});

			} catch(err) {
				_appServices.errorHandler.onError(err);
			}
		};

		var _uploadPicture = function(localFileUri) {

			try {

				alert("upload " + localFileUri);
			
				_localPictures.remove(localFileUri);

			} catch(err) {
				_appServices.errorHandler.onError(err);
			}
		};
		
		return {
			localPictures: _localPictures,
			takePicture: _takePicture,
			isCameraAvailable: _pictureTaker.isCameraAvailable,
			uploadPicture: _uploadPicture
		};
	});