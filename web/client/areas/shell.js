﻿define(['knockout', 'jquery', 'Q', 'plugins/router', 'app/services'],
	function(ko, $, Q, _router, _appServices) {

		var _isAuthorized = ko.observable(false);

		var _shellClasses = ko.computed(function() {

			var classes = '';

			classes += _appServices.errorHandler.lastErrorMessage() ? 'error' : '';

			return classes;
		});

		var _getSecurityContextOnActivate = function() {

			if (_currentSecurityContextRefreshOperation == undefined) {

				return _refreshSecurityContext();

			} else {

				return _currentSecurityContextRefreshOperation()
					.then(function() {

						if (!_isAuthorized()) {

							_refreshSecurityContext();
						}
					});
			}
		};

		var _currentSecurityContextRefreshOperation;

		var _refreshSecurityContext = function() {

			var loadContextTask = {};

			_currentSecurityContextRefreshOperation = Q(undefined);

			try {

				loadContextTask = _appServices.appTasks.pushTask('loading security context');

				_currentSecurityContextRefreshOperation = _appServices.securityContext.initContext()
					.then(function() {

						_isAuthorized(true);
					})
					.fail(function(error) {

						_appServices.errorHandler.onError(error);

					}).finally(function() {

						_appServices.appTasks.popTask(loadContextTask);
					});

			} catch(err) {

				_appServices.appTasks.popTask(loadContextTask);

				_appServices.errorHandler.onError(err);
			}

			return _currentSecurityContextRefreshOperation;
		};

		var _activate = function() {

			try {

				var initializePromise = _getSecurityContextOnActivate()
					.then(function() {
						return _router
							.map([
								{ route: ['', 'pictures'], moduleId: 'areas/pictures', title: 'pictures', nav: true, hash: '/pictures' },
								{ route: 'deployment', moduleId: 'areas/deployment', title: 'deployment', nav: true, hash: '/deployment' },
							])
							.buildNavigationModel()
							.mapUnknownRoutes('areas/pictures', '/')
							.activate({ pushState: true });
					});
				
				//let splash screen animation finish
				var minimumDelayPromise = Q.delay(500); 
				return Q.all([initializePromise, minimumDelayPromise])
					.spread(function(routerActivation, delay) {
						return routerActivation;
					})
				.fail(function (error) {
					_appServices.errorHandler.onError(error);
				});

			} catch(err) {
				_appServices.errorHandler.onError(err);
				return true;
			}
		};

		return {
			router: _router,
			activate: _activate,
			user: _appServices.securityContext.userInfo,
			shellClasses: _shellClasses,
			errorMessage: _appServices.errorHandler.lastErrorMessage,
			clearError: function() {
				_appServices.errorHandler.lastErrorMessage(null);
			},
			version: _appServices.config.version
	};
	});