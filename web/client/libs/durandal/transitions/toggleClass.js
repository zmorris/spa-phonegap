
define(['durandal/system', 'durandal/composition', 'jquery', 'Q'], function(system, composition, $, Q) {

	var toggleClass = function(context) {

		var _delayMilliseconds = context.transitionOptions ? context.transitionOptions.delayMilliseconds ? context.transitionOptions.delayMilliseconds : 0 : 0;
		var _classToToggle = context.transitionOptions ? context.transitionOptions.classToToggle ? context.transitionOptions.classToToggle : '' : '';

		return system.defer(function(deferred) {

			var showChild = function() {

				context.triggerAttach();

				Q.delay(_delayMilliseconds).then(function() {

					$(context.child).css({ display: 'block' });

					Q.delay(20).then(function() { //give contents a chance to adjust after 'display' change (i.e. chart container size)

						$(context.child).addClass(_classToToggle);

						deferred.resolve();
					});
				});
			};

			if (!context.child) {

				$(context.activeView).removeClass(_classToToggle);

			} else {

				if (context.activeView) {

					$(context.activeView).removeClass(_classToToggle);

					showChild();

				} else {

					showChild();
				}
			}
		}).promise();
	};

	return toggleClass;
});