﻿
requirejs.config({
	baseUrl: '/client',
	urlArgs: new Date().valueOf(),
	waitSeconds: 15,
	paths: {
		'text': './libs/text',
		'durandal': './libs/durandal',
		'plugins': './libs/durandal/plugins',
		'transitions': './libs/durandal/transitions',
		'jquery': './libs/jquery-1.9.1.min',
		'knockout': './libs/knockout-2.3.0',
		'Q': './libs/q.0.9.7.min',
		'linq': './libs/linq.require',
		'moment': './libs/moment.2.1.0.min',
	},
});

define(['app/deploymentSettings'],
	function(deployment) {

		requirejs.config({ urlArgs: deployment.version });

		require(['durandal/system', 'durandal/app', 'durandal/viewLocator', 'Q', 'app/config'],
			function(durandalSystem, durandalApp, durandalViewLocator, Q, config) {

				//durandalSystem.debug(false);
				durandalSystem.debug(true);

				durandalApp.configurePlugins({
					router: true,
					dialog: true,
					widget: true
				});

				durandalApp.title = 'SPAPICS';
				
				//set Durandal to use Q promises
				durandalSystem.defer = function(action) {
					var deferred = Q.defer();
					action.call(deferred, deferred);
					var promise = deferred.promise;
					deferred.promise = function() {
						return promise;
					};
					return deferred;
				};

				config.init(window.document);

				durandalApp.start().then(function() {

					durandalViewLocator.useConvention(null, '/client');

					durandalApp.setRoot('areas/shell', 'entrance');
				});
			});
	});