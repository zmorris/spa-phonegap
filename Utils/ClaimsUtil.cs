﻿using System.Security.Claims;
using System.Threading;

namespace Utils
{
	public static class ClaimsUtil
	{
		public static ClaimsPrincipal AddClaimToThreadPrincipal(string claimType, string claimValue)
		{
			return AddClaimsToThreadPrincipal(new ClaimsIdentity(new Claim[1]
			{
				new Claim(claimType, claimValue)
			}));
		}

		public static ClaimsPrincipal AddClaimsToThreadPrincipal(ClaimsIdentity identityToAdd)
		{
			var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
			if (claimsPrincipal == null)
			{
				claimsPrincipal = new ClaimsPrincipal(identityToAdd);
				Thread.CurrentPrincipal = claimsPrincipal;
				ClaimsPrincipal.ClaimsPrincipalSelector = () => (ClaimsPrincipal) Thread.CurrentPrincipal;
			}
			else
				claimsPrincipal.AddIdentity(identityToAdd);
			return claimsPrincipal;
		}
	}
}