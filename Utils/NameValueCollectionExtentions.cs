﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;

namespace Utils
{
    public static class NameValueCollectionExtentions
    {
        public static string GetWithException(this NameValueCollection source, string key)
        {
            string value = source.Get(key);

            if (string.IsNullOrEmpty(value))
            {
                throw new SettingsPropertyNotFoundException(string.Format(
                    "AppSetting '{0}' not found in configuration.", key));
            }

            return value;
        }

        public static Dictionary<string, string> ToDictionary(this NameValueCollection source)
        {
            return source.Cast<string>()
                         .Select(s => new {Key = s, Value = source[s]})
                         .ToDictionary(p => p.Key, p => p.Value);
        }
    }
}